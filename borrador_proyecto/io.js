
const fs = require('fs'); // cargamos  la librería fs sobre la constante fs

function writeUserDatatoFile(data) {
 console.log("writeUserDatatoFile");


 var jsonUserData = JSON.stringify(data); // stringfy para convertir a JSON. Lo guardamos en la variable jsonUserData

// Método de la librería fs con 4 parámetros (ruta/nombre fichero, datos, unicode, excepciones)
//  Encoding:  Unicode  UTF-8   (16 BITS)  Cubre todos los idiomas. Se puede codificar todo.
//                      ASCII (7 BITS)
//                      ANSI (8 BITS)

 fs.writeFile("./usuarios.json", jsonUserData, "utf8",
   function(err) {
     if(err) {
       console.log(err);
     } else {
       console.log("Usuario persistido");
     }
   }
 )
}

module.exports.writeUserDatatoFile = writeUserDatatoFile;
