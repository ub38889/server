// Con el REQUIRE importamos el fichero desde la ruta indicada
const io = require('../io');

const crypt = require('../crypt');

const requestJson = require('request-json');



const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujm10ed/collections/cuentas";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;// la clave ya no está en el código sino que es una referencia del fichero .env

console.log(mLabAPIKey);


function getCuentasByIdV2(req, res) { // devuelve las cuentas de un usuario con una id concreta
       console.log("GET /apitechu/v2/cuentas/:id");

       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                              //con la url  para acceder a la coleccion user


       var ident = req.params.id; //  la variable ident es el identificador que llega en el parámetro.
       //
       // EN params.userid y en q={} hay que poner el nombre externo
       // en el caso de query ponemos el mismo nombre que se haya definido en MLAB
       //
       var query = 'q={"IdUsuario":' + ident + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

       console.log("query1" + query);
       console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          if (err){
            var response  = {"msg" : "Error obteniendo las cuentas por id de usuarios"}
            res.status(500);
          }  else { if (body.length > 0 ){ // Si el body tiene contenido
                        var response = body;
                        console.log(body);

                    } else {
                        var response ={"msg" : "Usuario no encontrado"}
                        res.status(404);
                          }

                  }

          res.send(response); // Se envía el response
        });
        //  var response = !err ?    // condicion ternaria: Si no es error se responde el contenido del body y sino el mensaje de error
        //  body[0] : { "msg" : "Error obteniendo los usuarios"}
}



function getCuentasByIBANV2(req, res) { // Recupera una cuenta por un código IBAN

       console.log("get /apitechu/v2/cuentas/IBAN");

       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                              //con la url  para acceder a la coleccion user


       var identiban = req.params.IBAN; //  la variable ident es el identificador que llega en el parámetro.

       var query = 'q={"IBAN":' + identiban + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

       console.log("query1" + query);
       console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          if (err){
            var response  = {"msg" : "Error obteniendo las cuentas por IBAN"}
            res.status(500);
          }  else { if (body.length > 0 ){ // Si el body tiene contenido


                      var response = body;
                      console.log(body);


                    } else {
                        var response ={"msg" : "Cuenta no encontrado"}
                        res.status(404);
                          }

                  }

          res.send(response); // Se envía el response
        });
        //  var response = !err ?    // condicion ternaria: Si no es error se responde el contenido del body y sino el mensaje de error
        //  body[0] : { "msg" : "Error obteniendo los usuarios"}
}

function postCuentasByIBANV2(req, res) { // modifica el saldo de una cuenta por IBAN

       console.log("POST /apitechu/v2/cuentas");

       var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                              //con la url  para acceder a la coleccion user


       var identiban = req.body.IBAN; //  la variable ident es el identificador que llega en el parámetro.

       var query = 'q={"IBAN":' + identiban + '}'; // la variable query es la condicion de que la id sea la que llega por parámetro

       console.log("query1" + query);
       console.log("query2" + "cuentas?" + query + "&" + mLabAPIKey);
       //invocamos al get  con (la http de la coleccion user + ? condicion query + la APIKEY) y una funcion con tres parámetros
       httpClient.get("cuentas?" + query + "&" + mLabAPIKey, function(err, resMLab, body){
         // resMLab ES LA RESPUESTA DESDE EL SERVIDOR

          if (err){
            var response  = {"msg" : "Error obteniendo las cuentas por IBAN"}
            res.status(500);
          }  else { if (body.length > 0 ){ // Si el body tiene contenido


                    cuentas[0].Balance=cuentas[0].Balance + req.body.Importe;
                    cuentas[0].ContMovto ++;


                    } else {
                        var response ={"msg" : "Cuenta no encontrado"}
                        res.status(404);
                          }

                  }

          res.send(response); // Se envía el response
        });
        //  var response = !err ?    // condicion ternaria: Si no es error se responde el contenido del body y sino el mensaje de error
        //  body[0] : { "msg" : "Error obteniendo los usuarios"}
}

function createCuentaV2(req, res) {
         console.log("POST /apitechu/v2/cuentas");
         console.log("parámetros" );
         console.log( req.params);
         console.log("QueryString" );
         console.log( req.query);
         console.log("HEaders" );
         console.log(req.headers);
         console.log("Body" );
         console.log(req.body);




         var newCuenta = { // Recuperamos estos parámetros del Body
           "IdUsuario": req.body.IdUsuario,
           "IBAN": req.body.IBAN,
           "Concepto": req.body.Concepto,
           "Importe": req.body.Importe,
           "Fecha": req.body.Fecha


         }

         var httpClient = requestJson.createClient(baseMlabURL); // para activar la libreria tenemos que llamar a la función createClient
                                                                // con la url  para acceder a la coleccion user

         console.log(newMovto);
        // Para el post se necesitan tres parámetros: la http  , los datos del cliente y la funcion
         httpClient.post("movimientos?" + mLabAPIKey, newMovto, function(err, resMLab, body){
           console.log("movimiento creado en Mlab");
           res.status(201).send({"msg":"movimiento guardado"}); // entendemos que ha finalizado ok con code status 201
        })
}




// Se incluyen los module.exports de las funciones para que se puedan invocar desde fuera


module.exports.getCuentasByIdV2 = getCuentasByIdV2;
module.exports.getCuentasByIBANV2 = getCuentasByIBANV2;
module.exports.postCuentasByIBANV2 = postCuentasByIBANV2;
